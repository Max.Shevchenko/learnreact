import React, {Component} from "react"



class Header extends  Component {

    state = {
        active: 'active',
        keywords: "nothing"
    }

    inputChangeHandler = (event) => {
        const value = event.target.value === '' ? 'active' : 'non-active';
        this.setState({
        keywords: event.target.value,
            active: value
    })}
    render() {
        //
        // const style = {
        //     background: "red"
        // }
        //
        // if (this.state.keywords !== '') {
        //     style.background = 'blue'
        // } else {
        //     style.background = 'red'
        // }

        return (
            <header className={this.state.active}>
                <div
                    className="logo"
                    onClick={() => console.log("I was clicked")}
                >Logo</div>
                <input type="text" onChange={this.inputChangeHandler}/>
            </header>
        )
    }
}

export default Header